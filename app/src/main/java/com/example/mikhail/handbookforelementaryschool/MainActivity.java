package com.example.mikhail.handbookforelementaryschool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void onClick(View view){
        switch(view.getId()){
            case R.id.mainMathButton:
                Intent intent = new Intent(this, MathActivity.class);
                startActivity(intent);
                break;
            case R.id.mainRussianLangButton:
                // go to russianLangActivity
                break;
            case R.id.mainScienceButton:
                // go to scienceActivity
                break;
            case R.id.mainEnglishLangButton:
                // go to englishLangActivity
        }
    }
}
