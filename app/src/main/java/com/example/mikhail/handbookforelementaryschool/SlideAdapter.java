package com.example.mikhail.handbookforelementaryschool;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SlideAdapter  extends PagerAdapter {

    Context context;
    LayoutInflater inflater;
    //List of titles
    public int[] titleList={
            R.string.theme_1_title,
            R.string.theme_2_title,
            R.string.theme_3_title

    };

    // list of descriptions
    public int[] descriptionList={
            R.string.theme_1_description,
            R.string.theme_2_description,
            R.string.theme_3_description
    };

    public SlideAdapter (Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return titleList.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (ConstraintLayout)object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide,container,false);
        TextView titleText = (TextView) view.findViewById(R.id.mathTitle);
        TextView descriptionText = (TextView) view.findViewById((R.id.mathDescription));
        titleText.setText(titleList[position]);
        descriptionText.setText(descriptionList[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position,Object object) {
        container.removeView((ConstraintLayout)object);
    }
}
